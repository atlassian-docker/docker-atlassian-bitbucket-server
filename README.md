*NOTE*: This Docker image has been published as both `atlassian/bitbucket` and `atlassian/bitbucket-server` up until February 15, 2024.
Both names refer to the same image. However, post-February 15, 2024, the `atlassian/bitbucket-server` version ceased receiving updates, including both existing and new tags.
If you have been using `atlassian/bitbucket-server`, switch to the `atlassian/bitbucket` image to ensure access to the latest updates and new tags.

---

![Atlassian Bitbucket Server](https://wac-cdn.atlassian.com/dam/jcr:bf39fc40-3871-491f-98e3-fb2293f57a00/bitbucket-icon-gradient-blue.svg?cdnVersion=696)

Bitbucket Data Center is an on-premises source code management solution for Git
that's secure, fast, and enterprise grade. Create and manage repositories, set
up fine-grained permissions, and collaborate on code - all with the flexibility
of your servers.

Learn more about Bitbucket Data Center: <https://www.atlassian.com/software/bitbucket/enterprise/data-center>

# Overview

This Docker container makes it easy to get an instance of Bitbucket up and
running.

** NOTE: For backwards-compatibility, by default the image will start both
Bitbucket and an embedded OpenSearch. However this is not a recommended
configuration, especially in a clustered environment, and has known issues with
shutdown. instead, we recommend running a separate OpenSearch instance
(possibly in another Docker container); see below for instructions on connecting
to an external OpenSearch cluster. **

** If running this image in a production environment, we strongly recommend you
run this image using a specific version tag instead of latest. This is because
the image referenced by the latest tag changes often and we cannot guarantee
that it will be backwards compatible. **

** Use docker version >= 20.10.10 **

# Quick Start

For the `BITBUCKET_HOME` directory that is used to store the repository data
(amongst other things) we recommend mounting a host directory as a
[data volume](https://docs.docker.com/engine/tutorials/dockervolumes/#/data-volumes),
or via a named volume.

Additionally, if running Bitbucket in Data Center mode it is required that a
shared filesystem is mounted.

Volume permission is managed by entry scripts. To get started you can use a data
volume, or named volumes. In this example we'll use named volumes.

    $> docker volume create --name bitbucketVolume
    $> docker run -v bitbucketVolume:/var/atlassian/application-data/bitbucket --name="bitbucket" -d -p 7990:7990 -p 7999:7999 atlassian/bitbucket

Note that this command can substitute folder paths with named volumes.

Start Atlassian Bitbucket Data Center:

    $> docker run -v /data/bitbucket:/var/atlassian/application-data/bitbucket --name="bitbucket" -d -p 7990:7990 -p 7999:7999 atlassian/bitbucket

**Success**. Bitbucket is now available on [http://localhost:7990](http://localhost:7990)*

Please ensure your container has the necessary resources allocated to it.
We recommend 2GiB of memory allocated to accommodate both the application server
and the git processes.
See [Supported Platforms](https://confluence.atlassian.com/display/BitbucketServer/Supported+platforms) for further information.


_* Note: If you are using `docker-machine` on Mac OS X, please use `open http://$(docker-machine ip default):7990` instead._

# Advanced Usage
For advanced usage, e.g. configuration, troubleshooting, supportability, etc.,
please check the [**Full Documentation**](https://atlassian.github.io/data-center-helm-charts/containers/BITBUCKET/).

