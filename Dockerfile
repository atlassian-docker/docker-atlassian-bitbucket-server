ARG BASE_IMAGE=eclipse-temurin:17-noble
FROM $BASE_IMAGE

LABEL maintainer="dc-deployments@atlassian.com"
LABEL securitytxt="https://www.atlassian.com/.well-known/security.txt"

ARG BITBUCKET_VERSION

ENV APP_NAME                                        bitbucket
ENV RUN_USER                                        bitbucket
ENV RUN_GROUP                                       bitbucket
ENV RUN_UID                                         2003
ENV RUN_GID                                         2003

# https://confluence.atlassian.com/display/BitbucketServer/Bitbucket+Server+home+directory
ENV BITBUCKET_HOME                                  /var/atlassian/application-data/bitbucket
ENV BITBUCKET_INSTALL_DIR                           /opt/atlassian/bitbucket
ENV SEARCH_ENABLED                                  true
ENV APPLICATION_MODE                                default
ENV JRE_HOME                                        /opt/java/openjdk
ENV JAVA_BINARY                                     ${JRE_HOME}/bin/java

WORKDIR $BITBUCKET_HOME

# Expose HTTP and SSH ports
EXPOSE 7990
EXPOSE 7999

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends fontconfig openssh-client perl python3 python3-jinja2 tini \
    && apt-get clean autoclean && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

COPY bin/make-git.sh \
     bin/post-unpack-cleanup.sh \
     bin/log4shell-vulnerability-fix.sh \
     bin/git-versions.py \
     /

RUN /make-git.sh

COPY exec-bitbucket-node.sh _exec-webapp.sh ${BITBUCKET_INSTALL_DIR}/bin/

COPY entrypoint.py \
     shared-components/image/entrypoint_helpers.py \
     shutdown-wait.sh                               /
COPY shared-components/support                      /opt/atlassian/support

ARG DOWNLOAD_URL=https://product-downloads.atlassian.com/software/stash/downloads/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz

RUN groupadd --gid ${RUN_GID} ${RUN_GROUP} \
    && useradd --uid ${RUN_UID} --gid ${RUN_GID} --home-dir ${BITBUCKET_HOME} --shell /bin/bash ${RUN_USER} \
    && echo PATH=$PATH > /etc/environment \
    && mkdir -p                                     ${BITBUCKET_INSTALL_DIR} \
    && curl -fsSL ${DOWNLOAD_URL} -o /tmp/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz \
    && curl -fsSL ${DOWNLOAD_URL}.sha256 -o /tmp/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz.sha256 \
    && set -e; cd /tmp && sha256sum -c atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz.sha256 \
    && tar -xf /tmp/atlassian-bitbucket-${BITBUCKET_VERSION}.tar.gz --strip-components=1 -C "${BITBUCKET_INSTALL_DIR}" \
    && rm /tmp/atlassian-bitbucket* \
    && /post-unpack-cleanup.sh                      ${BITBUCKET_INSTALL_DIR} \
    && /log4shell-vulnerability-fix.sh \
    && chmod -R 550                                 ${BITBUCKET_INSTALL_DIR}/ \
    && chown -R ${RUN_USER}:root                    ${BITBUCKET_INSTALL_DIR}/ \
    && chmod 770                                    ${BITBUCKET_INSTALL_DIR}/*search/logs \
    && chown -R ${RUN_USER}:${RUN_GROUP}            ${BITBUCKET_HOME} \
    && sed -i '2i JVM_SUPPORT_RECOMMENDED_ARGS=$(eval echo \${JVM_SUPPORT_RECOMMENDED_ARGS})' ${BITBUCKET_INSTALL_DIR}/bin/start-bitbucket.sh \
    && for file in "/opt/atlassian/support /entrypoint.py /entrypoint_helpers.py /shutdown-wait.sh"; do \
       chmod -R "u=rwX,g=rX,o=rX" ${file} && \
       chown -R root. ${file}; done \
    && rm /make-git.sh /log4shell-vulnerability-fix.sh /post-unpack-cleanup.sh

VOLUME ["${BITBUCKET_HOME}"]

ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["/entrypoint.py", "--log=INFO"]
