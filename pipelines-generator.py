from pathlib import Path
import os
import jinja2 as j2

TEMPLATE_FILE = 'bitbucket-pipelines.yml.j2'
images = {
    'Bitbucket': {
        11: {
            'mac_key': 'bitbucket',
            'artefact': 'atlassian-bitbucket-software',
            'start_version': '8.9',
            'end_version': '9.0',
            'default_release': False,
            'base_image': 'eclipse-temurin:11-noble',
            'dockerfile': 'Dockerfile',
            'tag_suffixes': ['jdk11','ubuntu-jdk11'],
            'docker_repos': ['atlassian/bitbucket'],
            'batches': 36,
        },
        17: {
            'mac_key': 'bitbucket',
            'artefact': 'atlassian-bitbucket-software',
            'start_version': '8.9',
            'default_release': True,
            'base_image': 'eclipse-temurin:17-noble',
            'dockerfile': 'Dockerfile',
            'tag_suffixes': ['jdk17','ubuntu-jdk17'],
            'docker_repos': ['atlassian/bitbucket'],
            'batches': 39,
        },
        "17-ubi": {
            'mac_key': 'bitbucket',
            'artefact': 'atlassian-bitbucket-software',
            'start_version': '8.18',
            'default_release': False,
            'base_image': 'registry.access.redhat.com/ubi9/openjdk-17',
            'dockerfile': 'Dockerfile.ubi',
            'tag_suffixes': ['ubi9','ubi9-jdk17'],
            'docker_repos': ['atlassian/bitbucket'],
            'snyk_threshold': 'critical'
        },
        21: {
            'mac_key': 'bitbucket',
            'artefact': 'atlassian-bitbucket-software',
            'start_version': '9.3',
            'default_release': False,
            'base_image': 'eclipse-temurin:21-noble',
            'dockerfile': 'Dockerfile',
            'tag_suffixes': ['jdk21','ubuntu-jdk21'],
            'docker_repos': ['atlassian/bitbucket'],
        }
    }
}


def main():
    jenv = j2.Environment(
        loader=j2.FileSystemLoader('.'),
        lstrip_blocks=True,
        trim_blocks=True)
    template = jenv.get_template(TEMPLATE_FILE)
    generated_output = template.render(images=images, batches=12)

    print(generated_output)

if __name__ == '__main__':
    main()
