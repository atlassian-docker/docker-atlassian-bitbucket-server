#!/usr/bin/python3 -B

import re
import sys


support_matrix = {
    '6.0':      '2.20',
    '6.2':      '2.21',
    '6.4':      '2.22',
    '6.6':      '2.23',
    '6.8':      '2.24',
    '7.0':      '2.25',
    '7.2':      '2.26',
    '7.3':      '2.27',
    '7.5':      '2.28',
    '7.7':      '2.29',
    '7.9':      '2.30',
    '7.12':     '2.31',
    '7.14':     '2.32',
    '7.16':     '2.33',
    '7.19':     '2.34',
    '7.21':     '2.35',
    '8.0':      '2.36',
    '8.3':      '2.37',
    '8.7':      '2.38',
    '8.8':      '2.39',
    '8.9':      '2.40',
    '8.9.5':    '2.42',
    '8.9.11':   '2.42',
    '8.9.12':   '2.42',
    '8.9.17':   '2.42',
    '8.9.19':   '2.46',
    '8.9.21':   '2.47',
    '8.10':     '2.40',
    '8.14':     '2.42',
    '8.19.1':   '2.42',
    '8.19.6':   '2.42',
    '8.19.9':   '2.46',
    '8.19.11':  '2.47',
    '9.0':      '2.42',
    '9.1.1':    '2.46',
    '9.3':      '2.47',
}


def vsort(version):
    return tuple(int(i) for i in re.findall(r'\b\d+\b', version))


def supported_git_version(bitbucket_version):
    for v in sorted(support_matrix, key=lambda x: vsort(x), reverse=True):
        if vsort(bitbucket_version) >= vsort(v):
            return support_matrix[v]


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit('No bitbucket version specified')
    bitbucket_version = sys.argv[1]
    print(supported_git_version(bitbucket_version))
