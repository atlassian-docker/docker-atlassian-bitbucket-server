#!/bin/bash

set -e

SOURCE_DIR="/git_source"
DIST_DIR="/usr"

: ${BITBUCKET_VERSION=}
: ${SUPPORTED_GIT_VERSION=$(/git-versions.py $BITBUCKET_VERSION)}


mkdir -p ${DIST_DIR}

# Install build dependencies
echo "Installing git build dependencies"
if command -v microdnf &> /dev/null; then
  echo "UBI image detected"
  microdnf update -y
  microdnf install -y --setopt=install_weak_deps=0 git make autoconf gcc zlib-devel libcurl-devel openssl-devel expat-devel diffutils
else
  apt-get update
  apt-get install -y --no-install-recommends git dh-autoreconf libcurl4-gnutls-dev libexpat1-dev libssl-dev make zlib1g-dev
fi

# cut -c53- here drops the SHA (40), tab (1) and "refs/tags/v" (11), because some things, like the
# snapshot URL and tarball root directory, don't have the leading "v" from the tag in them
# Thanks to Bryan Turner for this improved method of retrieving the appropriate git version
GIT_VERSION=$(git ls-remote git://git.kernel.org/pub/scm/git/git.git | cut -c53- | grep "^${SUPPORTED_GIT_VERSION}\.[0-9\.]\+$" | sort -V | tail -n 1)
echo "Building git $GIT_VERSION for Bitbucket $BITBUCKET_VERSION"
curl -s -o - "https://git.kernel.org/pub/scm/git/git.git/snapshot/git-${GIT_VERSION}.tar.gz" | tar -xz --strip-components=1 --one-top-level="${SOURCE_DIR}"
cd "${SOURCE_DIR}"

# Uninstall pkg git
echo "Uninstalling default git"
if command -v microdnf &> /dev/null; then
  echo "UBI image detected"
  microdnf remove git-core-doc git-core less git perl-Git -y
else
  apt-get purge -y git
fi

# Install git from source
make configure
./configure --prefix=${DIST_DIR}
make -j`nproc` NO_TCLTK=1 NO_GETTEXT=1 install

# Remove and clean up dependencies
echo "Removing dependencies and cleaning up"
cd /
rm -rf ${SOURCE_DIR}
if command -v microdnf &> /dev/null; then
  echo "UBI image detected. Removing dependencies"
  microdnf remove make autoconf gcc zlib-devel libcurl-devel openssl-devel expat-devel diffutils libxcrypt-devel glibc-devel kernel-headers -y
  microdnf clean all
else
  apt-get purge -y dh-autoreconf
  apt-get clean autoclean
  apt-get autoremove -y
  rm -rf /var/lib/apt/lists/*
fi

echo "GIT VERSION **********************"
git --version
echo "GIT VERSION **********************"